﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SubSystemCanal
{
    public partial class Form1 : Form
    {
        EditorCanal editCanal;
        processCanal procCanal;
        public List<Point> click = new List<Point>();
        int[,] data, sortinfo;
        string[] fileText;
        int picksect = 0;

        public Form1()
        {
            InitializeComponent();
            openFileDialog1.Filter = "Text files(*.txt)|*.txt";
            saveFileDialog1.Filter = "Text files(*.txt)|*.txt";
            autobutton.Enabled = false; byhandbutton.Enabled = false;
            studybutton.Enabled = false; 
            checkBox1.Enabled = false; canalPicture.Enabled = false;
            savebutton.Enabled = false;
            canalPicture.MouseClick += pictureBox2_MouseClick;
        }

        private void open_file(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = openFileDialog1.FileName;
            fileText = System.IO.File.ReadAllLines(filename);            
            string[] elements;
            data = new int[fileText.Length,4];
            sortinfo = new int[fileText.Length, 4];
            for (int a = 0; a < fileText.Length; a++)
            {
                elements = fileText[a].Split(' ', ' ', ' ');                
                data[a,0] = Convert.ToInt32(elements[0]);
                data[a, 1] = Convert.ToInt32(elements[1]);
                data[a, 2] = Convert.ToInt32(elements[2]);
                data[a, 3] = Convert.ToInt32(elements[3]);
            }
            sortinfo = data;
            int temp0, temp1, temp2, temp3;
            for (int i = 0; i < fileText.Length - 1; i++)
            {
                for (int j = i + 1; j < fileText.Length; j++)
                {
                    if (data[i, 2] > data[j, 2])
                    {
                        temp0 = sortinfo[i, 0];
                        temp1 = sortinfo[i, 1];
                        temp2 = sortinfo[i, 2];
                        temp3 = sortinfo[i, 3];
                        data[i, 0] = sortinfo[j, 0];
                        data[i, 1] = sortinfo[j, 1];
                        data[i, 2] = sortinfo[j, 2];
                        data[i, 3] = sortinfo[j, 3];
                        sortinfo[j, 0] = temp0;
                        sortinfo[j, 1] = temp1;
                        sortinfo[j, 2] = temp2;
                        sortinfo[j, 3] = temp3;
                    }
                }
            }
            editCanal = new EditorCanal(sortinfo, fileText.Length, this);
            editCanal.DrawSections();
            procCanal = new processCanal(sortinfo, fileText.Length, this, editCanal);
            autobutton.Enabled = true; byhandbutton.Enabled = true;
            studybutton.Enabled = true; checkBox1.Enabled = true;
            openbutton.Enabled = false; Clear.Enabled = true;
        }

        private void save_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            int[,] saveResult;
            saveResult = new int[fileText.Length, 5];
            saveResult = procCanal.returnResult();
            string filename = saveFileDialog1.FileName;
            string save,saveall="";
            for (int i = 0; i < fileText.Length; i++)
            {
                save = Convert.ToString(saveResult[i, 0]) + " " + Convert.ToString(saveResult[i, 1]) + " " + Convert.ToString(saveResult[i, 2]) + " " + Convert.ToString(saveResult[i, 3]) + " " + Convert.ToString(saveResult[i, 4] + Environment.NewLine);
                saveall = String.Concat(saveall, save);
                save = "";                
            }
            System.IO.File.WriteAllText(filename, saveall);
            MessageBox.Show("Файл сохранен");
        }

        private void auto_Click(object sender, EventArgs e)
        {
            procCanal.autoCanal(0);
            byhandbutton.Enabled = false;
            savebutton.Enabled = true;
        }

        private void byhandbutton_Click(object sender, EventArgs e)
        {
            autobutton.Enabled = false;
            canalPicture.Enabled = true;
        }            

        private void pictureBox2_MouseClick(object sender, MouseEventArgs e)
        {
            if (canalPicture.PointToClient(System.Windows.Forms.Cursor.Position).X < canalPicture.Width / 2 - 10&&picksect==0)
            {
                procCanal.pickSection(canalPicture.PointToClient(System.Windows.Forms.Cursor.Position).X,
                    canalPicture.PointToClient(System.Windows.Forms.Cursor.Position).Y, picksect);
                picksect = 1;
            }
            else if(picksect==1)
            {
                procCanal.pickSection(canalPicture.PointToClient(System.Windows.Forms.Cursor.Position).X,
                    canalPicture.PointToClient(System.Windows.Forms.Cursor.Position).Y, picksect);
                picksect = 0;
            }
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            editCanal.clearAll();
            autobutton.Enabled = false; byhandbutton.Enabled = false;
            studybutton.Enabled = false; savebutton.Enabled = false;
            checkBox1.Enabled = false; canalPicture.Enabled = false;
            openbutton.Enabled = true; Clear.Enabled = false;
        }

        private void studybutton_Click(object sender, EventArgs e)
        {
            autobutton.Enabled = false; byhandbutton.Enabled = false;
            MessageBox.Show("Распределение отрезков в канале\n\n 1. Необходимо отсортировать отрезки по алгоритму левого конца\n 2. Разместить в каждом канале непересекающиеся отрезки");
            procCanal.autoCanal(1);
        }
    }
}
