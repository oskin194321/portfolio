﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubSystemCanal
{
    class GraphEditor
    {
        Form1 form;
        processCanal procCanal;
        int[,] graph;
        double[,] vertexs;
        public Bitmap bitmap;
        public List<Point> vertex = new List<Point>();
        Graphics drawing;
        int count, middle_x,middle_y;

        public GraphEditor(int[,] input, int count1, Form1 formm, processCanal procCan)
        {            
            this.form = formm;
            this.procCanal = procCan;
            bitmap = new Bitmap(form.canalPicture.Width, form.canalPicture.Height);
            count = count1;
            graph=new int[count,5];
            graph = input;
            vertexs = new double[count, 2];
        }

        public void drawGraph() 
        {
            drawing = Graphics.FromImage(bitmap);
            drawing.Clear(Color.White);
            middle_x = form.graphPicture.Width/2;
            middle_y = form.graphPicture.Height / 2;
            for(int i=0;i<count;i++)
            {
                double x = Math.Cos(2 * Math.PI * i / count) * 250 + middle_x;
                double y = Math.Sin(2 * Math.PI * i / count) * 180 + middle_y;
                vertexs[i,0]=x;
                vertexs[i,1]=y;
                drawing.FillEllipse(Brushes.Blue,(int)x,(int)y,20,20);
                drawing.DrawString(Convert.ToString(graph[i, 0]), new Font("Arial", 14), new SolidBrush(Color.Black),
                    new Point((int)x+10, (int)y-25));
                switch (graph[i,4])
                {
                    case 1:
                        drawing.FillEllipse(Brushes.Red, (int)x, (int)y, 20, 20);
                        break;
                    case 2:
                        drawing.FillEllipse(Brushes.Blue, (int)x, (int)y, 20, 20);
                        break;
                    case 3:
                        drawing.FillEllipse(Brushes.GreenYellow, (int)x, (int)y, 20, 20);
                        break;
                    case 4:
                        drawing.FillEllipse(Brushes.Gold, (int)x, (int)y, 20, 20);
                        break;
                    case 5:
                        drawing.FillEllipse(Brushes.Aqua, (int)x, (int)y, 20, 20);
                        break;
                    case 6:
                        drawing.FillEllipse(Brushes.Gray, (int)x, (int)y, 20, 20);
                        break;
                    default:
                        break;
                }
            }
            for (int i = 0; i < count - 1; i++) 
            {
                for (int j = i + 1; j < count; j++) 
                {
                    if (graph[i, 2] <= graph[j, 3] && graph[i, 3] >= graph[j, 2])
                    {
                        drawing.DrawLine(Pens.Black, (int)vertexs[i,0]+10, (int)vertexs[i,1]+10, (int)vertexs[j,0]+10, (int)vertexs[j,1]+10);
                    }
                }
            }
            form.graphPicture.Image = bitmap;
        }
    }
}
