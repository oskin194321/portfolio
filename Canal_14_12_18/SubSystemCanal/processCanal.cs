﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SubSystemCanal
{
    class processCanal
    {
        Form1 form;
        EditorCanal editcanal;
        GraphEditor grapheditor;
        int[,] sortinfo, autoresult,result;        
        int count, pick=0, n_section,n_result=0;

        public processCanal(int[,] input, int count1, Form1 formm,EditorCanal edit)
        {            
            this.form = formm;
            this.editcanal = edit;
            count = count1;
            sortinfo=new int[count,4];
            sortinfo = input;
            autoresult=new int[count,5];
            result = new int[count, 5];
        }

        public void autoCanal(int study1) 
        {
            int study = study1;
            int step = form.canalPicture.Height / 7;
            int level = step;
            int line_canal = 5;
            int done = 0, cross=0, number=1, newcanal=0; // уже в списке, пересечение отрезков, позиция в рез. списке, первый отрезок в новом канале
            int checkcanal = 0; // есть ли первый отрезок в новом канале    
            autoresult[0, 0] = sortinfo[0, 0];
            autoresult[0, 1] = level;
            autoresult[0, 2] = line_canal;
            autoresult[0, 3] = autoresult[0,2] + sortinfo[0, 3]-sortinfo[0,2];
            autoresult[0, 4] = 1;           
            for (int i = 1; i < count; i++) // каналы
            {
                if (autoresult[count-1, 0] == 0) // пока не все отрезки распределены 
                {
                    for (int j = 1; j < count; j++) // отрезки
                    {
                        for (int k = 0; k < count; k++) // наличие отрезка в результате
                        {
                            if (sortinfo[j, 0] == autoresult[k, 0])
                                done = 1;
                        }
                        if (done != 1) 
                        {
                            if (i > 1&&i!=checkcanal&&newcanal==0) 
                            {
                                autoresult[number, 0] = sortinfo[j, 0];
                                autoresult[number, 1] = level;
                                autoresult[number, 2] = line_canal + sortinfo[j, 2] - sortinfo[0, 2];
                                autoresult[number, 3] = autoresult[number, 2] + sortinfo[j, 3] - sortinfo[j, 2];
                                autoresult[number, 4] = i;
                                
                                newcanal = 1;
                                checkcanal = i;
                                number++;
                            }
                            if (newcanal == 0)
                            {
                                for (int m = 0; m < count; m++)
                                {
                                    if (sortinfo[j, 2] <= autoresult[m, 3] + sortinfo[0, 2] - 5 && 
                                        sortinfo[j, 3] >= autoresult[m, 2] + sortinfo[0, 2] - 5 && i==autoresult[m,4])
                                        cross = 1;
                                }
                                if (cross == 0)
                                {
                                    autoresult[number, 0] = sortinfo[j, 0];
                                    autoresult[number, 1] = level;
                                    autoresult[number, 2] = line_canal + sortinfo[j,2] - sortinfo[0,2];
                                    autoresult[number, 3] = autoresult[number, 2] + sortinfo[j, 3] - sortinfo[j, 2];
                                    autoresult[number, 4] = i;                                   
                                    number++;
                                }
                            }
                            cross = 0;
                        }
                        done = 0;
                        newcanal = 0;
                    }
                }
                level += step;
                line_canal = 5;
            }
            editcanal.DrawCanals(autoresult,study);
            grapheditor = new GraphEditor(autoresult, count, form, this);
            grapheditor.drawGraph();            
        }

        public void pickSection(int x_click, int y_click, int typ)
        {
            if (n_result < count)
            {
                int step = form.canalPicture.Height / 7, step1 = 0;
                int middle = form.canalPicture.Width / 2;
                int x = x_click, y = y_click, type = typ, have = 0;
                if (type == 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        if (x >= sortinfo[i, 2] && x <= sortinfo[i, 3] && y >= sortinfo[i, 1] - 10 && y <= sortinfo[i, 1] + 10)
                        {
                            editcanal.DrawPickSection(i);
                            n_section = i;
                            pick = 1;
                            break;
                        }
                    }
                }
                else if (pick == 1)
                {
                    for (int i = 0; i < 7; i++)
                    {
                        if (x >= middle && y >= step1 - 15 && y <= step1 + 15)
                        {
                            for (int j = 0; j < count; j++)
                            {
                                if (sortinfo[n_section, 0] == result[j, 0])
                                    have = 1;
                            }
                            if (have == 0)
                            {
                                result[n_result, 0] = sortinfo[n_section, 0];
                                result[n_result, 1] = step1;
                                result[n_result, 2] = 5 + sortinfo[n_section, 2] - sortinfo[0, 2];
                                result[n_result, 3] = result[n_result, 2] + sortinfo[n_section, 3] - sortinfo[n_section, 2];
                                result[n_result, 4] = i;
                                editcanal.DrawPickCanal(result[n_result, 1], result[n_result, 2], result[n_result, 3], result[n_result, 0]);
                                n_result++;
                                pick = 1;
                            }
                            have = 0;
                            break;
                        }
                        step1 += step;
                    }
                    pick = 0;
                }
            }
            else 
            {
                grapheditor = new GraphEditor(result, count, form, this);
                grapheditor.drawGraph();
            }
        }

        public int[,] returnResult() 
        {
            return autoresult;
        }
    }
}
