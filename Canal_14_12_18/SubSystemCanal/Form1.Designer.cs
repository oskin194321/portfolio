﻿namespace SubSystemCanal
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.Clear = new System.Windows.Forms.Button();
            this.byhandbutton = new System.Windows.Forms.Button();
            this.autobutton = new System.Windows.Forms.Button();
            this.savebutton = new System.Windows.Forms.Button();
            this.openbutton = new System.Windows.Forms.Button();
            this.canalPicture = new System.Windows.Forms.PictureBox();
            this.studybutton = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.graphPicture = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.button2 = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.canalPicture)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.graphPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1138, 573);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Cornsilk;
            this.tabPage1.Controls.Add(this.Clear);
            this.tabPage1.Controls.Add(this.byhandbutton);
            this.tabPage1.Controls.Add(this.autobutton);
            this.tabPage1.Controls.Add(this.savebutton);
            this.tabPage1.Controls.Add(this.openbutton);
            this.tabPage1.Controls.Add(this.canalPicture);
            this.tabPage1.Controls.Add(this.studybutton);
            this.tabPage1.Controls.Add(this.checkBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1130, 544);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Распределение отрезков в канале";
            // 
            // Clear
            // 
            this.Clear.BackColor = System.Drawing.Color.LightSalmon;
            this.Clear.Location = new System.Drawing.Point(973, 356);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(135, 44);
            this.Clear.TabIndex = 9;
            this.Clear.Text = "Очистить экран";
            this.Clear.UseVisualStyleBackColor = false;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // byhandbutton
            // 
            this.byhandbutton.Location = new System.Drawing.Point(973, 130);
            this.byhandbutton.Name = "byhandbutton";
            this.byhandbutton.Size = new System.Drawing.Size(135, 39);
            this.byhandbutton.TabIndex = 8;
            this.byhandbutton.Text = "Вручную";
            this.byhandbutton.UseVisualStyleBackColor = true;
            this.byhandbutton.Click += new System.EventHandler(this.byhandbutton_Click);
            // 
            // autobutton
            // 
            this.autobutton.Location = new System.Drawing.Point(973, 68);
            this.autobutton.Name = "autobutton";
            this.autobutton.Size = new System.Drawing.Size(135, 42);
            this.autobutton.TabIndex = 7;
            this.autobutton.Text = "Авто";
            this.autobutton.UseVisualStyleBackColor = true;
            this.autobutton.Click += new System.EventHandler(this.auto_Click);
            // 
            // savebutton
            // 
            this.savebutton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.savebutton.Location = new System.Drawing.Point(973, 458);
            this.savebutton.Name = "savebutton";
            this.savebutton.Size = new System.Drawing.Size(135, 45);
            this.savebutton.TabIndex = 6;
            this.savebutton.Text = "Сохранить";
            this.savebutton.UseVisualStyleBackColor = false;
            this.savebutton.Click += new System.EventHandler(this.save_Click);
            // 
            // openbutton
            // 
            this.openbutton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.openbutton.Location = new System.Drawing.Point(973, 6);
            this.openbutton.Name = "openbutton";
            this.openbutton.Size = new System.Drawing.Size(135, 43);
            this.openbutton.TabIndex = 5;
            this.openbutton.Text = "Открыть файл";
            this.openbutton.UseVisualStyleBackColor = false;
            this.openbutton.Click += new System.EventHandler(this.open_file);
            // 
            // canalPicture
            // 
            this.canalPicture.BackColor = System.Drawing.Color.White;
            this.canalPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.canalPicture.Location = new System.Drawing.Point(6, 6);
            this.canalPicture.Name = "canalPicture";
            this.canalPicture.Size = new System.Drawing.Size(942, 497);
            this.canalPicture.TabIndex = 4;
            this.canalPicture.TabStop = false;
            // 
            // studybutton
            // 
            this.studybutton.BackColor = System.Drawing.Color.Khaki;
            this.studybutton.Location = new System.Drawing.Point(973, 198);
            this.studybutton.Name = "studybutton";
            this.studybutton.Size = new System.Drawing.Size(135, 41);
            this.studybutton.TabIndex = 3;
            this.studybutton.Text = "Обучение";
            this.studybutton.UseVisualStyleBackColor = false;
            this.studybutton.Click += new System.EventHandler(this.studybutton_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoEllipsis = true;
            this.checkBox1.Location = new System.Drawing.Point(956, 245);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(168, 41);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "Учет вертикальных ограничений";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Lavender;
            this.tabPage2.Controls.Add(this.button7);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.graphPicture);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.button6);
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.checkBox2);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1130, 544);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Раскраска графа отрезков";
            // 
            // graphPicture
            // 
            this.graphPicture.BackColor = System.Drawing.Color.White;
            this.graphPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.graphPicture.Location = new System.Drawing.Point(6, 6);
            this.graphPicture.Name = "graphPicture";
            this.graphPicture.Size = new System.Drawing.Size(951, 497);
            this.graphPicture.TabIndex = 9;
            this.graphPicture.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(985, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 18);
            this.label1.TabIndex = 8;
            this.label1.Text = "Выберите цвет";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Gold;
            this.button6.Location = new System.Drawing.Point(1050, 95);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(25, 25);
            this.button6.TabIndex = 7;
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.GreenYellow;
            this.button5.Location = new System.Drawing.Point(1019, 95);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(25, 25);
            this.button5.TabIndex = 6;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Blue;
            this.button4.Location = new System.Drawing.Point(1050, 55);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(25, 25);
            this.button4.TabIndex = 5;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Red;
            this.button3.Location = new System.Drawing.Point(1019, 55);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(25, 25);
            this.button3.TabIndex = 4;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(970, 444);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(153, 21);
            this.checkBox2.TabIndex = 3;
            this.checkBox2.Text = "Просмотр каналов";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Khaki;
            this.button2.Location = new System.Drawing.Point(970, 394);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(152, 44);
            this.button2.TabIndex = 2;
            this.button2.Text = "Обучение";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // colorDialog1
            // 
            this.colorDialog1.Color = System.Drawing.Color.Azure;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Aqua;
            this.button1.Location = new System.Drawing.Point(1019, 135);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(25, 25);
            this.button1.TabIndex = 10;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Gray;
            this.button7.Location = new System.Drawing.Point(1050, 135);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(25, 25);
            this.button7.TabIndex = 11;
            this.button7.UseVisualStyleBackColor = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1138, 571);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.Text = "CanalGraph";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.canalPicture)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.graphPicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button studybutton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button savebutton;
        private System.Windows.Forms.Button openbutton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button byhandbutton;
        private System.Windows.Forms.Button autobutton;
        public System.Windows.Forms.PictureBox canalPicture;
        public System.Windows.Forms.PictureBox graphPicture;
        private System.Windows.Forms.Button Clear;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button1;
    }
}

