﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SubSystemCanal
{
    class EditorCanal
    {
        Form1 form;
        int[,] info, autodraw;
        public Bitmap bitmap;
        public List<Point> sections = new List<Point>();
        Graphics drawing;
        int count, middle,section=-1;

        public EditorCanal(int[,] input, int count1, Form1 formm)
        {            
            this.form = formm;
            bitmap = new Bitmap(form.canalPicture.Width, form.canalPicture.Height);
            count = count1;
            info=new int[count,4];
            info = input;
            autodraw = new int[count, 5];
        }

        public void DrawSections() 
        {
            drawing = Graphics.FromImage(bitmap);
            drawing.Clear(Color.White);            
            for (int i = 0, j = 0; i < count; i++) 
            {                
                sections.Add(new Point(info[i,2], info[i,1]));
                sections.Add(new Point(info[i, 3], info[i, 1]));
                drawing.DrawLine(Pens.Black, sections[j].X, sections[j].Y - 4, sections[j].X, sections[j].Y + 4);
                drawing.DrawLine(Pens.Black, sections[j], sections[j+1]);
                drawing.DrawLine(Pens.Black, sections[j+1].X, sections[j+1].Y - 4, sections[j+1].X, sections[j+1].Y + 4);
                drawing.DrawString(Convert.ToString(info[i,0]), new Font("Arial", 11), new SolidBrush(Color.Blue), 
                    new Point((sections[j+1].X-sections[j].X)/2+sections[j].X-2,sections[j].Y-20));
                j += 2;
            }
            int step = form.canalPicture.Height/7;
            int step1 = 0;
            middle=form.canalPicture.Width/2;
            int line_canal=0;
            for (int i = 0; i < 6; i++) 
            {
                step1 += step;
                while(line_canal<middle-5)
                {
                    drawing.DrawLine(Pens.Gray,middle+line_canal,step1,middle+line_canal+5,step1);
                    line_canal+=10;
                }
                line_canal = 0;
            }
            drawing.DrawLine(Pens.Black, middle-10, 0, middle-10, step+form.canalPicture.Height);
            form.canalPicture.Image = bitmap;
        }

        public void DrawCanals(int[,] autores,int study1)
        {
            int study = study1;
            autodraw = autores;
            drawing = Graphics.FromImage(bitmap);
            for (int i = 0; i < count; i++)
            {
                if (study == 1)
                    MessageBox.Show("Размещаем " + autodraw[i, 0] + " отрезок на " + autodraw[i, 4] + " магистраль");

                drawing.DrawLine(Pens.Black, autodraw[i, 2] + middle, autodraw[i, 1] - 4, autodraw[i, 2] + middle, autodraw[i, 1] + 4);
                drawing.DrawLine(Pens.Black, autodraw[i, 2] + middle, autodraw[i, 1], autodraw[i, 3] + middle, autodraw[i, 1]);
                drawing.DrawLine(Pens.Black, autodraw[i, 3] + middle, autodraw[i, 1] - 4, autodraw[i, 3] + middle, autodraw[i, 1] + 4);
                drawing.DrawString(Convert.ToString(autodraw[i, 0]), new Font("Arial", 11), new SolidBrush(Color.Blue),
                    new Point((autodraw[i, 3] - autodraw[i, 2]) / 2 + autodraw[i, 2] - 2 + middle, autodraw[i, 1] - 20));

                form.canalPicture.Image = bitmap;
            }
        }

        public void DrawPickSection(int sect) 
        {
            if(section!=-1)
                drawing.DrawLine(Pens.Black, sections[section * 2], sections[section * 2 + 1]);  
            section = sect;
            drawing = Graphics.FromImage(bitmap);
            drawing.DrawLine(Pens.Red, sections[section*2], sections[section*2 + 1]);                   
            form.canalPicture.Image = bitmap;

        }

        public void DrawPickCanal(int y,int x1,int x2,int num)
        {
            drawing.DrawLine(Pens.Black, x1 + middle, y - 4, x1 + middle, y + 4);
            drawing.DrawLine(Pens.Black, x1 + middle, y, x2 + middle, y);
            drawing.DrawLine(Pens.Black, x2 + middle, y - 4, x2 + middle, y + 4);
            drawing.DrawString(Convert.ToString(num), new Font("Arial", 9), new SolidBrush(Color.Blue),
                new Point((x2 - x1) / 2 + x1 - 2 + middle, y - 15));
            form.canalPicture.Image = bitmap;
        }

        public void clearAll() 
        {        
            drawing.Clear(Color.White);  
            form.canalPicture.Image = bitmap;
        }
    }
}
