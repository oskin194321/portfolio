﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairyCave
{
    class Hero
    {
        public Texture2D spriteWalk, spriteHero, spriteShot;
        public Rectangle rect, border, rectangle;
        Game1 game;
        Level level;
        Menu menu;
        int FrameCount, CurrFrame;
        float TimeForFrame, TotalTime;
        bool isRight, isLeft, isUp, isDown;
        public bool prevLeft;
        SpriteEffects effect = new SpriteEffects();

        public Hero(int AnimationSpeed,Game1 game,Level level, Menu menu)
        {
            this.level = level;
            this.game = game;
            this.menu = menu;
            rect = new Rectangle(45, 335, 50, 50);
            spriteHero = game.Content.Load<Texture2D>("Textures//stand2");
            spriteWalk = game.Content.Load<Texture2D>("Textures//walkk");
            spriteShot = game.Content.Load<Texture2D>("Textures//Shot");
            CurrFrame = 0;
            TimeForFrame = (float)1 / AnimationSpeed;
            TotalTime = 0;
            effect = SpriteEffects.FlipHorizontally;
        }

        public void Update(GameTime gameTime)
        {
            FrameCount = spriteWalk.Width / spriteWalk.Height;
            TotalTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (TotalTime > TimeForFrame)
            {
                CurrFrame++;
                CurrFrame = CurrFrame % (FrameCount - 1);
                TotalTime -= TimeForFrame;
                border=rect;
                int shift = 5 * gameTime.ElapsedGameTime.Milliseconds / 10; // speed * time
                if (isRight)
                    border.Offset(shift, 0);
                else if (isLeft)
                    border.Offset(-shift, 0);
                else if (isUp)
                    border.Offset(0, -shift);
                else if (isDown)
                    border.Offset(0, shift);
                if (!isRight && !isLeft&&!isUp&&!isDown)
                    border.Offset(0, 0);
                if (border.Left > 0 && border.Right < 1280 && border.Top > 0 && border.Bottom < 720 && !level.CollidesBox(border) && !level.CollidesRock(border) && !level.CollidesChest(border) /*&& !Create.CollidesEnemy(border)*/)                  
                    rect = border;
                if (rect.X > 1220&&level.lvl==1)
                {
                    rect = new Rectangle(1, 335, 50, 50);
                    level.LevelTwo();
                }

                if (rect.Y < 20 &&rect.X>0&&level.lvl == 2)
                {
                    level.WinGame();
                    menu.CurrScreen = 7;
                }

                if(menu.CurrScreen==5)
                    rect = new Rectangle(45, 335, 50, 50);
                if (level.lives == 0 || (game.currentTime > 500000 && level.lvl == 2))
                {
                    rect = new Rectangle(-50, -50, 50, 50);
                    if(level.lvl==1||level.lvl==2)
                    level.WinGame();
                }
                level.CollidesAbility(rect);
            }
        }

        public void DrawAnimation(SpriteBatch spriteBatch, bool Right,bool Left, bool Up, bool Down)
        {
            isRight = Right;
            isLeft = Left;
            isUp = Up;
            isDown = Down;
            int frameWidht = spriteWalk.Width / FrameCount;
            rectangle = new Rectangle(frameWidht * CurrFrame, 0, frameWidht, spriteWalk.Height);
            if (Left)
            {
                if (!level.CollidesEnemy_1(rect)&&!level.CollidesEnemy_2(rect))
                    spriteBatch.Draw(spriteWalk, rect, rectangle, Color.White, 0, Vector2.Zero, effect, 0);
                else
                    spriteBatch.Draw(spriteWalk, rect, rectangle, Color.Red, 0, Vector2.Zero, effect, 0);
            }
            else if (Right || Up || Down)
            {
                if (!level.CollidesEnemy_1(rect) && !level.CollidesEnemy_2(rect))
                    spriteBatch.Draw(spriteWalk, rect, rectangle, Color.White);
                else
                    spriteBatch.Draw(spriteWalk, rect, rectangle, Color.Red);
            }
            else if (prevLeft) 
            {
                if (!level.CollidesEnemy_1(rect) && !level.CollidesEnemy_2(rect))
                    spriteBatch.Draw(spriteHero, rect, rectangle, Color.White, 0, Vector2.Zero, effect, 0);
                else
                    spriteBatch.Draw(spriteHero, rect, rectangle, Color.Red, 0, Vector2.Zero, effect, 0);
            }
            else
                if (!level.CollidesEnemy_1(rect) && !level.CollidesEnemy_2(rect))
                    spriteBatch.Draw(spriteHero, rect, rectangle, Color.White);
                else
                    spriteBatch.Draw(spriteHero, rect, rectangle, Color.Red);

            if (Left)
                prevLeft = isLeft;
            else if (Right || Up || Down)
                prevLeft = false;
        }
    }
}
