﻿using FairyCave;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairyCave
{
    public class Enemy_1 : Enemy
    {
        public Enemy_1(Level level, Game1 game, SpriteBatch spriteBatch, ref Texture2D texture, Point position)
            : base(level, game, spriteBatch, ref texture, position)
        {
            speed = 2;
            hp = 50;
            hpnow = hpfull;
            hpfull = 50;
        }
        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (hpnow == hp)
            {
                spriteBatch.Draw(texture, rec, Color.White);
                spriteBatch.DrawString(fonthp, hp.ToString() + " / " + hpfull.ToString(), new Vector2(rec.X - 3, rec.Y - 15), Color.White);
            }
            else 
            {
                hpnow = hp;
                spriteBatch.Draw(texture, rec, Color.Red);
                spriteBatch.DrawString(fonthp, hp.ToString() + " / " + hpfull.ToString(), new Vector2(rec.X - 3, rec.Y - 15), Color.Red);
            }
        }

        protected override void Motion(GameTime time)
        {
            if (!level.CollidesBox(rec) && !level.CollidesRock(rec))
                Up();
            else
            {
                speed *= -1;
                Up();
            }
            //if (rec.Y <= 900)
            //    Down();
            //foreach (var obj in game.listOvals)
            //{
            //    if (rec.Intersects(obj.rec)/* && obj != prevQuad*/)
            //    {
            //        //prevQuad = obj;
            //        Remove();
            //    }
            //}
        }
        public void Remove()
        {
            game.onUpdate -= Update;
            game.onDraw -= Draw;
        }
    }
}

