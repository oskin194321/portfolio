﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairyCave
{
    public class Arrow_1 : Arrow
    {
        SpriteEffects effect = new SpriteEffects();

        public Arrow_1(Game1 game, Level level, SpriteBatch spriteBatch, ref Texture2D texture, Point position, bool direction)
            : base(game, level, spriteBatch, ref texture, position, direction)
        {
            this.spriteBatch = spriteBatch;
            this.texture = texture;
            effect = SpriteEffects.FlipHorizontally;
            speed = 5;
        }
        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Rectangle rectangle = new Rectangle(0, 0, texture.Width, texture.Height);
            if(!direction)
                spriteBatch.Draw(texture, rec, Color.White);
            else
                spriteBatch.Draw(texture, rec, rectangle, Color.White, 0, Vector2.Zero, effect, 0);
        }

        protected override void Motion()
        {
            level.Arrow_1Collides(this, rec);

            if (direction && rec.X >= -50 && rec.X < 1300)
                Left();
            else if (rec.X >= 10 && rec.X < 1300)
                Right();
        }

        public void Remove()
        {
            game.onUpdate -= Update;
            game.onDraw -= Draw;
        }
    }
}
