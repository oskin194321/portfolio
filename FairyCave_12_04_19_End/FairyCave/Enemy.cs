﻿using FairyCave;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairyCave
{
    public abstract class Enemy
    {
        protected GameTime time;
        protected Game1 game;
        protected Level level;
        protected Texture2D texture;
        protected SpriteFont fonthp;
        protected Point position;
        protected SpriteBatch spriteBatch;
        protected int speed, hpfull, hpnow;
        public int hp;
        public Rectangle rec;
        protected static readonly Random rnd = new Random();

        public Enemy(Level level, Game1 game, SpriteBatch spriteBatch, ref Texture2D texture, Point position)
        {
            this.game = game;
            this.level = level;
            this.texture = texture;
            this.position = position;
            this.spriteBatch = spriteBatch;
            rec = new Rectangle(position, new Point(40));
            fonthp = game.Content.Load<SpriteFont>("fonthp");
            game.onUpdate += Update;
            game.onDraw += Draw;
        }

        protected abstract void Motion(GameTime time);

        protected void Update(GameTime gameTime)
        {
            time = gameTime;
            if(game.CurrScreen==1)
              Motion(time);
        }

        protected abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);

        protected void Up()
        { rec.Y -= speed; }

        protected void Down()
        { rec.Y += speed; }

        protected void Left()
        { rec.X -= speed; }

        public void Right()
        { rec.X += speed; }

        protected void SideDiag()
        { rec.X -= speed; rec.Y += speed; }

        protected void MainDiag()
        { rec.X += speed; rec.Y += speed; }
    }
}
