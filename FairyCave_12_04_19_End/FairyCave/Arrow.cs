﻿using FairyCave;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairyCave
{
    public abstract class Arrow
    {
        protected GameTime time;
        protected Game1 game;
        protected Level level;
        protected Texture2D texture;
        protected Point position;
        protected SpriteBatch spriteBatch;
        protected int speed;
        protected bool direction;
        public Rectangle rec;

        public Arrow(Game1 game, Level level, SpriteBatch spriteBatch, ref Texture2D texture, Point position, bool direction)
        {
            this.game = game;
            this.texture = texture;
            this.position = position;
            this.spriteBatch = spriteBatch;
            this.direction = direction;
            this.level = level;
            rec = new Rectangle(position, new Point(25));
            game.onUpdate += Update;
            game.onDraw += Draw;
        }

        protected abstract void Motion();

        protected void Update(GameTime gameTime)
        {
            time = gameTime;
            if(game.CurrScreen==1)
            Motion();
        }

        protected abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);

        protected void Up()
        { rec.Y -= speed; }

        protected void Down()
        { rec.Y += speed; }

        protected void Left()
        { rec.X -= speed; }

        public void Right()
        { rec.X += speed; }

        protected void SideDiag()
        { rec.X -= speed; rec.Y += speed; }

        protected void MainDiag()
        { rec.X += speed; rec.Y += speed; }
    }
}
