﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairyCave
{
    class Camera
    {
        public Matrix transform;
        Viewport view;
        Vector2 centre;

        public Camera(Viewport newView) 
        {
            view = newView;
        }

        public void Update(GameTime gameTime, Hero ship)
        {
            if (ship.rect.X + (ship.rect.Width / 2)>300 && ship.rect.Y + (ship.rect.Height / 2) > 300
                && ship.rect.X + (ship.rect.Width / 2)<980 && ship.rect.Y + (ship.rect.Height / 2) < 420) // в центре
            {
                centre = new Vector2(ship.rect.X + (ship.rect.Width / 2) - 300, ship.rect.Y + (ship.rect.Height / 2) - 300);
                transform = Matrix.CreateScale(new Vector3(1, 1, 0)) *
                    Matrix.CreateTranslation(new Vector3(-centre.X, -centre.Y, 0));
            }
            else if (ship.rect.X + (ship.rect.Width / 2) < 300 && ship.rect.Y + (ship.rect.Height / 2) > 300 && ship.rect.Y + (ship.rect.Height / 2) < 420) // слева
            {
                centre = new Vector2(0, ship.rect.Y + (ship.rect.Height / 2) - 300);
                transform = Matrix.CreateScale(new Vector3(1, 1, 0)) *
                    Matrix.CreateTranslation(new Vector3(-centre.X, -centre.Y, 0));
            }
            else if (ship.rect.X + (ship.rect.Width / 2) > 980 && ship.rect.Y + (ship.rect.Height / 2) > 300 && ship.rect.Y + (ship.rect.Height / 2) < 420) // справа
            {
                centre = new Vector2(680, ship.rect.Y + (ship.rect.Height / 2) - 300);
                transform = Matrix.CreateScale(new Vector3(1, 1, 0)) *
                    Matrix.CreateTranslation(new Vector3(-centre.X, -centre.Y, 0));
            }
            else if (ship.rect.X + (ship.rect.Width / 2) > 300 && ship.rect.X + (ship.rect.Width / 2) < 980 && (ship.rect.Y + (ship.rect.Height / 2) < 300)) // сверху
            {
                centre = new Vector2(ship.rect.X + (ship.rect.Width / 2) - 300, 0);
                transform = Matrix.CreateScale(new Vector3(1, 1, 0)) *
                    Matrix.CreateTranslation(new Vector3(-centre.X, -centre.Y, 0));
            }
            else if (ship.rect.X + (ship.rect.Width / 2) > 300 && ship.rect.X + (ship.rect.Width / 2) < 980 && ship.rect.Y + (ship.rect.Height / 2) > 420) // снизу
            {
                centre = new Vector2(ship.rect.X + (ship.rect.Width / 2) - 300, 120);
                transform = Matrix.CreateScale(new Vector3(1, 1, 0)) *
                    Matrix.CreateTranslation(new Vector3(-centre.X, -centre.Y, 0));
            }
        }
    }
}
