﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairyCave
{
    class Background
    {
        Game1 game;
        Level Create;
        public Texture2D backTexture, Texture2;
        public Vector2 backPosition;

        public Background(Game1 game) 
        {
            backPosition = new Vector2(0, 0);
            backTexture = game.Content.Load<Texture2D>("background1");
            Texture2 = game.Content.Load<Texture2D>("background2");
        }

        public void LoadContent(ContentManager Content, String texture)
        {
            backTexture = Content.Load<Texture2D>(texture);
        }

        public void Draw(SpriteBatch spriteBatch,int lvl)
        {
            if(lvl==1)
                spriteBatch.Draw(backTexture, backPosition, Color.White);
            else
                spriteBatch.Draw(Texture2, backPosition, Color.White);
        }

    }
}
