﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairyCave
{
    public class Enemy_3 : Enemy
    {
        Rectangle rectnow, border;
        SoundEffect enemy;
        double distance;
        protected int shift = 20;

        public Enemy_3(Level level, Game1 game, SpriteBatch spriteBatch, ref Texture2D texture, Point position)
            : base(level, game, spriteBatch, ref texture, position)
        {
            speed = 2;
            hp = 350;
            hpnow = hpfull;
            hpfull = 350;
            rec = new Rectangle(position, new Point(70));
            enemy=game.Content.Load<SoundEffect>("enemy");
        }
        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (hpnow == hp)
            {
                if (hpnow < hpfull&&game.currentTime%500==0)
                {
                    hpnow++;
                    hp = hpnow;
                }
                if (game.currentTime % 4000 == 0)
                    enemy.Play();
                spriteBatch.Draw(texture, rec, Color.Red);
                spriteBatch.DrawString(fonthp, hp.ToString() + " / " + hpfull.ToString(), new Vector2(rec.X + 10, rec.Y - 15), Color.White);
            }
            else
            {
                hpnow = hp;
                spriteBatch.Draw(texture, rec, Color.Gray);
                spriteBatch.DrawString(fonthp, hp.ToString() + " / " + hpfull.ToString(), new Vector2(rec.X + 10, rec.Y - 15), Color.White);
            }
        }

        protected override void Motion(GameTime time)
        {
            if (Math.Abs(rec.X - rectnow.X) < 200 && Math.Abs(rec.Y - rectnow.Y) < 200)
            {
                border = rec;
                distance = Math.Sqrt((rectnow.X + shift - border.X) * (rectnow.X + shift - border.X) + (rectnow.Y + shift - border.Y) * (rectnow.Y + shift - border.Y));
                if (distance > 2)
                {
                    border.X += Convert.ToInt32(0.08 * time.ElapsedGameTime.Milliseconds * (rectnow.X + shift - border.X) / distance);
                    border.Y += Convert.ToInt32(0.08 * time.ElapsedGameTime.Milliseconds * (rectnow.Y + shift - border.Y) / distance);
                }
                if (!level.CollidesBox(border) && !level.CollidesRock(border))
                    rec = border;
            }
            else if (!level.CollidesBox(rec) && !level.CollidesRock(rec))
                Up();
            else
            {
                speed *= -1;
                Up();
            }
        }

        public void herorect(Rectangle rect)
        {
            rectnow = rect;
        }

        public void Remove()
        {
            game.onUpdate -= Update;
            game.onDraw -= Draw;
        }
    }
}
