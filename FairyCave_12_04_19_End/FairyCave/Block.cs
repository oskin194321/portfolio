﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairyCave
{
    public abstract class Block
    {
        protected GameTime time;
        protected Game1 game;
        public Level Create;
        protected Texture2D texture;
        protected Point position;
        protected SpriteBatch spriteBatch;
        public Rectangle rec;

        public Block(Game1 game, SpriteBatch spriteBatch, ref Texture2D texture, Point position)
        {
            this.game = game;
            this.texture = texture;
            this.position = position;
            this.spriteBatch = spriteBatch;
            rec = new Rectangle(position, new Point(40));
            game.onUpdate += Update;
            game.onDraw += Draw;
        }

        protected abstract void fall();

        protected void Update(GameTime gameTime)
        {
            time = gameTime;
            fall();
        }

        protected abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);        
    }
}
