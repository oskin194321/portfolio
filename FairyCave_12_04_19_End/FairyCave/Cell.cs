﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairyCave
{
    public class Cell : Block
    {
        public Rectangle Position { get; set; }

        public Cell(Game1 game, SpriteBatch spriteBatch, ref Texture2D texture, Point position)
            : base(game, spriteBatch, ref texture, position)
        {
            this.spriteBatch = spriteBatch;
            this.texture = texture;
        }

        protected override void fall()
        {
            //if (Create.scores == 3)
            //    Remove();
        }

        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, rec, Color.White);
        }

        public void Remove()
        {
            game.onUpdate -= Update;
            game.onDraw -= Draw;
            //Create.rocks.Remove(this);
        }
    }
}
