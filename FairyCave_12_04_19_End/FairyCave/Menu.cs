﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairyCave
{
    class Menu
    {
        Game1 game;
        Button playButton, resumeButton, statButton, exitButton, startButton;
        KeyboardState prevstates, nowstates;
        MouseState mouse, prevmouse;
        Song menusong, playsong;
        const byte MENU = 0, PLAY = 1, GAMEOVER = 2, EXIT = 3, STAT = 4, NEWPLAY = 5, INFO = 6, WIN=7;
        public int CurrScreen = MENU;

        public Menu(Game1 game) 
        {
            this.game = game;
            exitButton = new Button(new Rectangle(50, 400, 240, 42), true);
            statButton = new Button(new Rectangle(50, 300, 270, 42), true);
            resumeButton = new Button(new Rectangle(50, 200, 470, 42), true);
            playButton = new Button(new Rectangle(50, 100, 400, 42), true);
            startButton = new Button(new Rectangle(50, 300, 250, 42), true);
            menusong = game.Content.Load<Song>("menusong");
            playsong = game.Content.Load<Song>("playsong");
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(menusong);
        }

        public void Update(GameTime gameTime, MouseState mouse, KeyboardState states) 
        {
            prevstates = nowstates;
            nowstates = states;
            switch (CurrScreen)
            {
                case MENU:
                    if (playButton.update(new Vector2(mouse.X, mouse.Y)) == true && mouse != prevmouse && mouse.LeftButton == ButtonState.Pressed)
                        CurrScreen = NEWPLAY;
                    if (resumeButton.update(new Vector2(mouse.X, mouse.Y)) == true && mouse != prevmouse && mouse.LeftButton == ButtonState.Pressed && game.currentTime > 0)
                    { 
                        CurrScreen = PLAY;
                        MediaPlayer.Stop();
                        MediaPlayer.Play(playsong);
                    }
                    if (statButton.update(new Vector2(mouse.X, mouse.Y)) == true && mouse != prevmouse && mouse.LeftButton == ButtonState.Pressed)
                        CurrScreen = STAT;
                    if (exitButton.update(new Vector2(mouse.X, mouse.Y)) == true && mouse != prevmouse && mouse.LeftButton == ButtonState.Pressed)
                        CurrScreen = EXIT;
                    break;
                case NEWPLAY:
                    if (states.IsKeyDown(Keys.Escape))
                    {
                        CurrScreen = MENU;
                    }
                    if (startButton.update(new Vector2(mouse.X, mouse.Y)) == true && mouse != prevmouse && mouse.LeftButton == ButtonState.Pressed)
                    {
                        CurrScreen = PLAY;
                        MediaPlayer.Stop();
                        MediaPlayer.Play(playsong);
                    }
                    break;
                case PLAY:
                    if (nowstates.IsKeyDown(Keys.Escape) && prevstates.IsKeyUp(Keys.Escape))
                    {
                        MediaPlayer.Stop();
                        MediaPlayer.Play(menusong);
                        CurrScreen = MENU;
                    }
                    if (states.IsKeyDown(Keys.I))
                        CurrScreen = INFO;
                    break;
                case STAT:
                    if (states.IsKeyDown(Keys.Escape))
                        CurrScreen = MENU;
                    break;
                case EXIT:
                    game.Exit();
                    break;
                case INFO:
                    if (states.IsKeyDown(Keys.Escape))
                    {
                        CurrScreen = PLAY;
                        MediaPlayer.Stop();
                        MediaPlayer.Play(playsong);
                    }
                    break;
                case WIN:
                    if (states.IsKeyDown(Keys.Escape))
                    {
                        CurrScreen = MENU;
                        MediaPlayer.Stop();
                        MediaPlayer.Play(menusong);
                    }
                    break;
                case GAMEOVER:
                    break;
            }
            prevmouse = mouse;
        }
    }
}
