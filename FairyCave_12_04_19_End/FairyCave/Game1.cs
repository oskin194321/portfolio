﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace FairyCave
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Hero hero;
        Level level;
        Menu menu;
        DrawGame drawgame;
        Background backTexture;
        KeyboardState states, prevstates, nowstates;
        MouseState mouse;
        Camera camera;
        const byte MENU = 0, PLAY = 1, GAMEOVER = 2, EXIT = 3, STAT = 4, NEWPLAY=5, INFO=6, WIN=7;
        public int CurrScreen = MENU;
        public string nameNow = ""; 
        public delegate void onUPDATE(GameTime gameTime);
        public delegate void onDRAW(GameTime gameTime, SpriteBatch spriteBatch);
        public onDRAW onDraw;
        public onUPDATE onUpdate;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferHeight = 640; 
            graphics.PreferredBackBufferWidth = 600;
            Content.RootDirectory = "Content";
            menu = new Menu(this);
        }

        protected override void Initialize()
        {
            camera = new Camera(GraphicsDevice.Viewport);
            this.IsMouseVisible = true;            
            base.Initialize();
            Window.TextInput -= TextInputHandler;
            Window.TextInput += TextInputHandler;
        }

        private void TextInputHandler(object sender, TextInputEventArgs args)
        {
            if (menu.CurrScreen == NEWPLAY)
            {
                var pressedKey = args.Key;
                var character = args.Character;
                if (Char.IsControl(character))
                {
                    if (nameNow.Length > 0)
                        nameNow = nameNow.Remove(nameNow.Length - 1);
                }
                else if (nameNow.Length<20&&character.ToString()!=" ")
                    nameNow += character;
            }
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            backTexture = new Background(this);
            level = new Level(this, spriteBatch);
            hero = new Hero(30, this, level, menu);
            drawgame = new DrawGame(this, spriteBatch, level);
            level.LevelOne();
        }    
        
        protected override void UnloadContent() { }

        public int currentTime, cutoff=0;

        protected override void Update(GameTime gameTime)
        { 
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                Exit();
            mouse = Mouse.GetState();
            states = Keyboard.GetState();
            prevstates = nowstates;
            nowstates = Keyboard.GetState();
            hero.Update(gameTime);
            level.Update(gameTime);
            menu.Update(gameTime, mouse, states);
            CurrScreen = menu.CurrScreen;
            if (menu.CurrScreen == PLAY&&level.lives>0)
                currentTime += gameTime.ElapsedGameTime.Milliseconds;
            if (level.lvl == 2 && cutoff == 0) 
            {
                currentTime = 0;
                cutoff = 1;
            }
            if (menu.CurrScreen == NEWPLAY || menu.CurrScreen == WIN)
            {
                currentTime = 0;
                cutoff = 0;
            }
            camera.Update(gameTime, hero);              
            onUpdate.Invoke(gameTime);
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.BurlyWood); 
            switch (menu.CurrScreen)
            {
                case MENU:
                    spriteBatch.Begin();
                    drawgame.DrawMenu(spriteBatch);
                    break;
                case NEWPLAY:
                    spriteBatch.Begin();
                    drawgame.DrawNick(spriteBatch);
                    level.NewGameLevel();
                    break;
                case PLAY:
                    spriteBatch.Begin(SpriteSortMode.Deferred,
                        BlendState.AlphaBlend,
                        null, null, null, null,
                        camera.transform);
                    backTexture.Draw(spriteBatch, level.lvl);
                    onDraw.Invoke(gameTime, spriteBatch);
                    if (nowstates.IsKeyDown(Keys.F) && prevstates.IsKeyUp(Keys.F)&&level.arrowlvl==2)
                    {
                        if (level.cldown == 0)
                            level.cooldown();
                    }
                    if (level.cldown > 0)
                        level.cooldown();
                    if (states.IsKeyDown(Keys.D))
                        hero.DrawAnimation(spriteBatch, true, false, false, false);
                    else if (states.IsKeyDown(Keys.A))
                        hero.DrawAnimation(spriteBatch, false, true, false, false);
                    else if (states.IsKeyDown(Keys.W))
                        hero.DrawAnimation(spriteBatch, false, false, true, false);
                    else if (states.IsKeyDown(Keys.S))
                        hero.DrawAnimation(spriteBatch, false, false, false, true);
                    else if (nowstates.IsKeyDown(Keys.R) && prevstates.IsKeyUp(Keys.R))
                    {
                        level.shot(hero.rect, hero.prevLeft);
                        hero.DrawAnimation(spriteBatch, false, false, false, false);
                    }
                    else
                        hero.DrawAnimation(spriteBatch, false, false, false, false);
                    spriteBatch.End();
                    spriteBatch.Begin();
                    drawgame.DrawBarGame(spriteBatch);
                    if (level.CollidesRock(hero.border))
                        drawgame.DrawWarning(spriteBatch);
                    break;
                case STAT:
                    spriteBatch.Begin();
                    drawgame.DrawTable(spriteBatch);
                    break;
                case INFO:
                    spriteBatch.Begin();
                    backTexture.Draw(spriteBatch, level.lvl);
                    drawgame.DrawInfo(spriteBatch);
                    break;
                case WIN:
                    spriteBatch.Begin();
                    drawgame.DrawWin(spriteBatch);
                    break;
                case EXIT:               
                    break;                
                case GAMEOVER: 
                    break;
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}