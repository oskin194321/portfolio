﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairyCave
{
    class DrawGame
    {
        Game1 game;
        Level level;
        SpriteBatch spriteBatch;
        Texture2D menubgr, toolbar, heart, score, timer, guide, gameback, wasd, stand1, ability1, ability2, r, backwin;
        SpriteFont font, font_point, textfont;
        protected int timediff=0, timediff2=0, time=0, time1=0;
        public string[,] data, sortinfo;

        public DrawGame(Game1 game, SpriteBatch spriteBatch, Level level) 
        {
            this.game=game;
            this.spriteBatch=spriteBatch;
            this.level = level;
            font = game.Content.Load<SpriteFont>("fontt");
            font_point = game.Content.Load<SpriteFont>("fontpoints");
            textfont = game.Content.Load<SpriteFont>("fonthp");
            stand1 = game.Content.Load<Texture2D>("textures//stand1");
            menubgr = game.Content.Load<Texture2D>("titles//menubackgr");
            toolbar = game.Content.Load<Texture2D>("toolbar");
            heart = game.Content.Load<Texture2D>("heart");
            score = game.Content.Load<Texture2D>("score");
            timer = game.Content.Load<Texture2D>("timer");
            guide = game.Content.Load<Texture2D>("guide");
            gameback = game.Content.Load<Texture2D>("gameback");
            wasd = game.Content.Load<Texture2D>("wasd");
            ability1 = game.Content.Load<Texture2D>("ability1");
            ability2 = game.Content.Load<Texture2D>("ability2");
            r = game.Content.Load<Texture2D>("r");
            backwin = game.Content.Load<Texture2D>("backwin");
            open_file();
        }

        public void DrawMenu(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(menubgr, new Rectangle(0, 0, menubgr.Width, menubgr.Height+40), Color.White);
            spriteBatch.DrawString(font, "новая игра", new Vector2(50, 100), Color.Aqua);
            if(game.currentTime>0)
                spriteBatch.DrawString(font, "продолжить", new Vector2(50, 200), Color.White);
            spriteBatch.DrawString(font, "рейтинг", new Vector2(50, 300), Color.CornflowerBlue);
            spriteBatch.DrawString(font, "выход", new Vector2(50, 400), Color.Red);
            spriteBatch.DrawString(font, "FairyCave", new Vector2(115, 10), Color.Goldenrod);
        }

        public void DrawNick(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(menubgr, new Rectangle(0, 0, menubgr.Width, menubgr.Height + 40), Color.White);
            spriteBatch.DrawString(font, "FairyCave", new Vector2(115, 10), Color.Goldenrod);
            spriteBatch.DrawString(font, "Введите ник:", new Vector2(50, 100), Color.Aqua);

            spriteBatch.DrawString(font, "_______________", new Vector2(50, 190), Color.White);
            spriteBatch.DrawString(font_point, "(Не более 20 символов)", new Vector2(50, 240), Color.Aqua);
            spriteBatch.DrawString(font_point, game.nameNow, new Vector2(60, 205), Color.White);

            spriteBatch.DrawString(font, "Играть", new Vector2(50, 300), Color.Red);
        }

        public void DrawBarGame(SpriteBatch spriteBatch)
        {
            if (game.currentTime < 5000 && level.lvl == 1) 
            {
                spriteBatch.Draw(gameback, new Rectangle(25, 10, gameback.Width, gameback.Height-70), Color.GreenYellow);
                spriteBatch.DrawString(font, "Уровень 1", new Vector2(130, 25), Color.White);
                spriteBatch.DrawString(font_point, "- В клетке -", new Vector2(220, 90), Color.White);
                spriteBatch.Draw(gameback, new Rectangle(320, 180, gameback.Width-300, gameback.Height - 60), Color.White);
                spriteBatch.Draw(wasd, new Rectangle(385, 195, wasd.Width-90, wasd.Height-60), Color.White);
                spriteBatch.DrawString(font_point, "Управление", new Vector2(370, 280), Color.White);
                spriteBatch.Draw(gameback, new Rectangle(25, 350, gameback.Width, gameback.Height), Color.Bisque);
                spriteBatch.DrawString(font_point, "    Что? Ничего не помнишь?\nТы провалился в пещеру. Поздравляю.\nВыясни, как выбраться отсюда.\nБудь осторожен, здесь опасно. Удачи!", new Vector2(60, 380), Color.White);
            }

            if (level.arrowlvl == 1 && timediff >= 0 && timediff < 4000)
            {
                if (time == 0)
                    time = game.currentTime;
                timediff = game.currentTime - time;
                spriteBatch.Draw(gameback, new Rectangle(25, 440, gameback.Width, gameback.Height - 80), Color.Bisque);
                spriteBatch.DrawString(font_point, "Получен навык 'Стрельба'", new Vector2(140, 460), Color.Red);
                spriteBatch.DrawString(font_point, "Нажми 'R' для выстрела", new Vector2(145, 510), Color.White);
            }
            else if(level.arrowlvl==0)
            {
                timediff = 0;
                time = 0;
            }

            if (level.arrowlvl == 2 && timediff2 >= 0 && timediff2 < 4000)
            {
                if (time1 == 0)
                    time1 = game.currentTime;
                timediff2 = game.currentTime - time1;
                spriteBatch.Draw(gameback, new Rectangle(25, 440, gameback.Width, gameback.Height - 80), Color.Bisque);
                spriteBatch.DrawString(font_point, "Получен навык 'Огненная стрела'", new Vector2(110, 460), Color.Red);
                spriteBatch.DrawString(font_point, "Нажми 'F' для активации", new Vector2(135, 510), Color.White);
            }
            else if (level.arrowlvl == 0)
            {
                timediff2 = 0;
                time1 = 0;
            }

            if (game.currentTime < 3000 && level.lvl == 2)
            {
                spriteBatch.Draw(gameback, new Rectangle(25, 10, gameback.Width, gameback.Height-70), Color.GreenYellow);
                spriteBatch.DrawString(font, "Уровень 2", new Vector2(130, 25), Color.White);
                spriteBatch.Draw(gameback, new Rectangle(25, 350, gameback.Width, gameback.Height), Color.Bisque);
                spriteBatch.DrawString(font_point, "  Где же выход?!\nСрочно выясни, как выбраться.\nОсторожно с местными существами,\nони чувствуют твое присутствие!", new Vector2(60, 380), Color.White);
            }

            if (level.lives == 0||(game.currentTime>500000&&level.lvl==2)) 
            {
                spriteBatch.DrawString(font, "Ты проиграл!", new Vector2(90, 10), Color.Red);
                spriteBatch.Draw(gameback, new Rectangle(25, 70, gameback.Width, gameback.Height - 40), Color.IndianRed);
                spriteBatch.Draw(stand1, new Rectangle(400, 90, stand1.Width - 300, stand1.Height - 300), Color.White);
                spriteBatch.DrawString(font_point, "Результат", new Vector2(140, 90), Color.White);
                spriteBatch.Draw(timer, new Rectangle(100, 130, 40, 40), Color.White);
                spriteBatch.DrawString(font_point, "Время: " + level.timeres.ToString(), new Vector2(150, 140), Color.White);
                spriteBatch.Draw(score, new Rectangle(100, 180, 40, 40), Color.White);
                spriteBatch.DrawString(font_point, "Очки: " + level.winscore.ToString(), new Vector2(150, 190), Color.White);
                if (level.newres == 1)
                {
                    int size = data.Length / 2 - 1;
                    level.newres = 0;
                    data[size, 0] = level.resname;
                    data[size, 1] = level.winscore.ToString();
                    sortfile();
                }
                if (level.resname != null)
                {
                    for (int i = 0; i < data.Length / 2; i++)
                    {
                        if (data[i, 0] == level.resname)
                        {
                            if (i > 4)
                            {
                                spriteBatch.Draw(gameback, new Rectangle(40, 520, gameback.Width - 30, gameback.Height - 150), Color.LemonChiffon);
                                spriteBatch.DrawString(font_point, i + 1 + ". '" + data[i, 0] + "'", new Vector2(100, 535), Color.Red);
                                spriteBatch.DrawString(font_point, data[i, 1], new Vector2(400, 535), Color.Red);
                            }
                            break;
                        }
                    }
                }
                for (int i = 240; i < 480; i += 50)
                    spriteBatch.Draw(gameback, new Rectangle(40, i, gameback.Width - 30, gameback.Height - 150), Color.LemonChiffon);
                for (int i = 0; i < 5; i++)
                {
                    if (data[i, 0] == level.resname)
                        spriteBatch.DrawString(font_point, i + 1 + ". '" + data[i, 0] + "'", new Vector2(100, 255 + i * 50), Color.Red);
                    else
                        spriteBatch.DrawString(font_point, i + 1 + ". '" + data[i, 0] + "'", new Vector2(100, 255 + i * 50), Color.White);
                    spriteBatch.DrawString(font_point, data[i, 1], new Vector2(400, 255 + i * 50), Color.White);
                }
                spriteBatch.DrawString(font_point, "'Esc' - вернуться в меню", new Vector2(150, 600), Color.White);
            }
            spriteBatch.Draw(toolbar, new Rectangle(102, 580, toolbar.Width, toolbar.Height), Color.White);
            spriteBatch.Draw(heart, new Rectangle(130, 584, 35, 35), Color.White);
            spriteBatch.Draw(score, new Rectangle(225, 584, 35, 35), Color.White);
            if (level.arrowlvl == 1) 
            {
                spriteBatch.Draw(ability1, new Rectangle(290, 584, 35, 35), Color.White);
                spriteBatch.DrawString(font_point, "R", new Vector2(330, 590), Color.White);
            }
            spriteBatch.Draw(guide, new Rectangle(500, 580, 40, 40), Color.White);
            if (level.lvl == 2)
            {
                spriteBatch.Draw(timer, new Rectangle(385, 584, 35, 35), Color.White);
                spriteBatch.DrawString(font_point, (500 - game.currentTime / 1000).ToString(), new Vector2(425, 590), Color.White);
                if (level.arrowlvl == 2) 
                {
                    if(level.cldown==0)
                        spriteBatch.Draw(ability2, new Rectangle(290, 584, 35, 35), Color.White);
                    if (level.cldown == 1&&level.timediff<5000) 
                    {
                        spriteBatch.Draw(ability2, new Rectangle(290, 584, 35, 35), Color.Blue);
                        spriteBatch.DrawString(font_point, ((6000-level.timediff)/1000).ToString(), new Vector2(300, 590), Color.White);
                    }
                    else if (level.cldown == 1 && level.timediff>5000 && level.timediff < 14000)
                    {
                        spriteBatch.Draw(ability2, new Rectangle(290, 584, 35, 35), Color.Gray);
                        spriteBatch.DrawString(font_point, ((15000 - level.timediff) / 1000).ToString(), new Vector2(300, 590), Color.White);
                    }
                    spriteBatch.DrawString(font_point, "F(R)", new Vector2(330, 590), Color.White);
                }
            }
            spriteBatch.DrawString(font_point, level.lives.ToString(), new Vector2(170, 590), Color.Red);
            spriteBatch.DrawString(font_point, level.scores.ToString(), new Vector2(265, 590), Color.White);
        }

        public void DrawInfo(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, "FairyCave", new Vector2(115, 10), Color.White);
            spriteBatch.Draw(gameback, new Rectangle(25, 80, gameback.Width, gameback.Height+100), Color.White);
            spriteBatch.Draw(stand1, new Rectangle(300, 100, stand1.Width-160, stand1.Height-160), Color.White);
            spriteBatch.Draw(heart, new Rectangle(70, 160, 40, 40), Color.White);
            spriteBatch.DrawString(font_point, level.lives.ToString(), new Vector2(120, 170), Color.White);
            spriteBatch.Draw(score, new Rectangle(70, 240, 40, 40), Color.White);
            spriteBatch.DrawString(font_point, level.scores.ToString(), new Vector2(120, 250), Color.White);

            spriteBatch.Draw(gameback, new Rectangle(25, 390, gameback.Width, gameback.Height - 20), Color.IndianRed);
            spriteBatch.Draw(wasd, new Rectangle(85, 420, wasd.Width - 90, wasd.Height - 60), Color.White);
            spriteBatch.DrawString(font_point, "Управление", new Vector2(70, 510), Color.White);
            if (level.arrowlvl == 1) 
            {
                spriteBatch.Draw(ability1, new Rectangle(320, 430, ability1.Width-90, ability1.Height-90), Color.White);
                spriteBatch.DrawString(font_point, "+", new Vector2(400, 450), Color.White);
                spriteBatch.Draw(r, new Rectangle(430, 440, r.Width-60, r.Height-60), Color.White);
            }
            else if (level.arrowlvl == 2)
            {
                spriteBatch.Draw(ability2, new Rectangle(320, 430, ability1.Width - 90, ability1.Height - 90), Color.White);
                spriteBatch.DrawString(font_point, "+", new Vector2(400, 450), Color.White);
                spriteBatch.Draw(r, new Rectangle(430, 440, r.Width - 60, r.Height - 60), Color.White);
                spriteBatch.DrawString(font_point, "(F)", new Vector2(485, 450), Color.White);
            }
            else
                spriteBatch.DrawString(font_point, "Нет", new Vector2(370, 450), Color.Yellow);
            spriteBatch.DrawString(font_point, "Особый навык", new Vector2(305, 510), Color.White);
            spriteBatch.DrawString(font_point, "'Esc' - вернуться в игру", new Vector2(160, 590), Color.White);
        }

        public void DrawWin(SpriteBatch spriteBatch) 
        {
            spriteBatch.Draw(backwin, new Rectangle(0, 0, backwin.Width, backwin.Height), Color.White);
            spriteBatch.DrawString(font, "Ты победил!", new Vector2(90, 10), Color.Red);
            spriteBatch.Draw(gameback, new Rectangle(25, 70, gameback.Width, gameback.Height - 40), Color.IndianRed);
            spriteBatch.Draw(stand1, new Rectangle(400, 90, stand1.Width - 300, stand1.Height - 300), Color.White);
            spriteBatch.DrawString(font_point, "Результат", new Vector2(140, 90), Color.White);
            spriteBatch.Draw(timer, new Rectangle(100, 130, 40, 40), Color.White);
            spriteBatch.DrawString(font_point, "Время: " + level.timeres.ToString(), new Vector2(150, 140), Color.White);
            spriteBatch.Draw(score, new Rectangle(100, 180, 40, 40), Color.White);
            spriteBatch.DrawString(font_point, "Очки: " + level.winscore.ToString(), new Vector2(150, 190), Color.White);
            if (level.newres == 1)
            {
                int size = data.Length / 2 - 1;
                level.newres = 0;
                data[size, 0] = level.resname;
                data[size, 1] = level.winscore.ToString();
                sortfile();
            }
            if (level.resname != null)
            {
                for (int i = 0; i < data.Length / 2; i++)
                {
                    if (data[i, 0] == level.resname)
                    {
                        if (i > 4)
                        {
                            spriteBatch.Draw(gameback, new Rectangle(40, 520, gameback.Width - 30, gameback.Height - 150), Color.LemonChiffon);
                            spriteBatch.DrawString(font_point, i + 1 + ". '" + data[i, 0] + "'", new Vector2(100, 535), Color.Red);
                            spriteBatch.DrawString(font_point, data[i, 1], new Vector2(400, 535), Color.Red);
                        }
                        break;
                    }
                }
            }
            for (int i = 240; i < 480; i += 50)
                spriteBatch.Draw(gameback, new Rectangle(40, i, gameback.Width - 30, gameback.Height - 150), Color.LemonChiffon);
            for (int i = 0; i < 5; i++)
            {
                if (data[i, 0] == level.resname)
                    spriteBatch.DrawString(font_point, i + 1 + ". '" + data[i, 0] + "'", new Vector2(100, 255 + i * 50), Color.Red);
                else
                    spriteBatch.DrawString(font_point, i + 1 + ". '" + data[i, 0] + "'", new Vector2(100, 255 + i * 50), Color.White);
                spriteBatch.DrawString(font_point, data[i, 1], new Vector2(400, 255 + i * 50), Color.White);
            }
            spriteBatch.DrawString(font_point, "'Esc' - вернуться в меню", new Vector2(150, 600), Color.White);
        }

        public void DrawTable(SpriteBatch spriteBatch) 
        {
            spriteBatch.DrawString(font, "Рейтинг игроков", new Vector2(10, 20), Color.SaddleBrown);
            spriteBatch.Draw(gameback, new Rectangle(25, 90, gameback.Width, gameback.Height-100), Color.LemonChiffon);
            spriteBatch.DrawString(font, "Игрок", new Vector2(60, 110), Color.White);
            spriteBatch.DrawString(font, "Очки", new Vector2(370, 110), Color.White);
            for(int i=210;i<500;i+=60)
                spriteBatch.Draw(gameback, new Rectangle(40, i, gameback.Width-30, gameback.Height -150), Color.LemonChiffon);
            if (level.resname != null)
            {
                for (int i = 0; i < data.Length / 2; i++)
                {
                    if (data[i, 0] == level.resname)
                    {
                        if (i > 4)
                        {
                            spriteBatch.Draw(gameback, new Rectangle(40, 510, gameback.Width - 30, gameback.Height - 150), Color.LemonChiffon);
                            spriteBatch.DrawString(font_point, i + 1 + ". '" + data[i, 0] + "'", new Vector2(100, 525), Color.Red);
                            spriteBatch.DrawString(font_point, data[i, 1], new Vector2(400, 525), Color.Red);
                        }
                        break;
                    }
                }
            }
            spriteBatch.DrawString(font_point, "'Esc' - вернуться в меню", new Vector2(150, 600), Color.White);
            for (int i = 0; i < 5; i++)
            {
                if(data[i,0]==level.resname)
                    spriteBatch.DrawString(font_point, i+1+". '" + data[i,0] + "'", new Vector2(100, 225+i*60), Color.Red);
                else
                    spriteBatch.DrawString(font_point, i + 1 + ". '" + data[i, 0] + "'", new Vector2(100, 225 + i * 60), Color.White);
                spriteBatch.DrawString(font_point, data[i,1], new Vector2(400, 225+i*60), Color.White);
            }
        }

        protected void open_file()
        {
            string[] fileText;
            fileText = System.IO.File.ReadAllLines("score.txt");
            string[] elements;
            data = new string[fileText.Length+10, 2];
            sortinfo = new string[fileText.Length+10, 2];
            for (int a = 0; a < fileText.Length; a++)
            {
                elements = fileText[a].Split(' ');
                data[a, 0] = elements[0];
                data[a, 1] = elements[1];
            }
            sortfile();
        }

        protected void sortfile()
        {            
            sortinfo = data;
            string temp0, temp1;
            for (int i = 0; i < data.Length/2; i++)
            {
                for (int j = i + 1; j < data.Length/2; j++)
                {
                    if (Convert.ToInt32(data[i, 1]) < Convert.ToInt32(data[j, 1]))
                    {
                        temp0 = sortinfo[i, 0];
                        temp1 = sortinfo[i, 1];
                        data[i, 0] = sortinfo[j, 0];
                        data[i, 1] = sortinfo[j, 1];
                        sortinfo[j, 0] = temp0;
                        sortinfo[j, 1] = temp1;
                    }
                }
            }
            data = sortinfo;
            savefile();
        }

        protected void savefile() 
        {
            string save, saveall = "";
            for (int i = 0; i < data.Length/2; i++)
            {
                if (data[i, 0] != null)
                {
                    save = data[i, 0] + " " + (data[i, 1] + Environment.NewLine);
                    saveall = String.Concat(saveall, save);
                    save = "";
                }
            }
            System.IO.File.WriteAllText("score.txt", saveall);
        }

        public void DrawWarning(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(toolbar, new Rectangle(102, 50, toolbar.Width, toolbar.Height), Color.GreenYellow);
            spriteBatch.DrawString(font_point, "Где же ключи? Хм...", new Vector2(170, 60), Color.White);
        }
    }
}