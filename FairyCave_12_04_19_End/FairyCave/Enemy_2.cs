﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairyCave
{
    public class Enemy_2 : Enemy
    {
        Rectangle rectnow, border;
        double distance;  
        protected int shift=10;

        public Enemy_2(Level level, Game1 game, SpriteBatch spriteBatch, ref Texture2D texture, Point position)
            : base(level, game, spriteBatch, ref texture, position)
        {
            speed = 3;
            hp = 90;
            hpnow = hpfull;
            hpfull = 90;
        }
        protected override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (hpnow == hp)
            {
                spriteBatch.Draw(texture, rec, Color.White);
                spriteBatch.DrawString(fonthp, hp.ToString() + " / " + hpfull.ToString(), new Vector2(rec.X - 3, rec.Y - 15), Color.White);
            }
            else 
            {
                hpnow = hp;
                spriteBatch.Draw(texture, rec, Color.Red);
                spriteBatch.DrawString(fonthp, hp.ToString() + " / " + hpfull.ToString(), new Vector2(rec.X - 3, rec.Y - 15), Color.Red);
            }
        }

        protected override void Motion(GameTime time)
        {
            if (Math.Abs(rec.X - rectnow.X) < 220 && Math.Abs(rec.Y - rectnow.Y) < 220)
            {
                border = rec;
                distance = Math.Sqrt((rectnow.X+shift - border.X) * (rectnow.X+shift - border.X) + (rectnow.Y+shift - border.Y) * (rectnow.Y+shift - border.Y));
                if (distance > 2)
                {
                    border.X += Convert.ToInt32(0.06 * time.ElapsedGameTime.Milliseconds * (rectnow.X+shift - border.X) / distance);
                    border.Y += Convert.ToInt32(0.06 * time.ElapsedGameTime.Milliseconds * (rectnow.Y+shift - border.Y) / distance);
                }
                if (!level.CollidesBox(border) && !level.CollidesRock(border))
                    rec = border;
            }
            else if (!level.CollidesBox(rec) && !level.CollidesRock(rec))
                Up();
            else
            {
                speed *= -1;
                Up();
            }
        }

        public void herorect(Rectangle rect) 
        {
            rectnow = rect;
        }

        public void Remove()
        {
            game.onUpdate -= Update;
            game.onDraw -= Draw;
        }
    }
}
