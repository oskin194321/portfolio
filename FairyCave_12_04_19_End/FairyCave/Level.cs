﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FairyCave
{
    public class Level
    {
        Game1 game;
        Arrow_1 arrow;
        Arrow_2 arrow2;
        Enemy_3 boss;
        SpriteBatch spriteBatch;
        Texture2D boxture1, boxture2, rock, sunduk1, sunduk2, saw, enemy1, arrowpic, arrowpic2, cell, ability1, ability2;
        protected int livesfull=250;
        public int lives=250, scores=0, lvl=1, arrowlvl=0, scoreres=0, timeres=0, winscore=0, newres=0;
        public string resname;
        public List<Blocks> boxs;
        public List<Blocks> abilities;
        public List<Cell> cells;
        public List<Chest> chests, chests_new;
        public List<Enemy_1> enemies;
        public List<Enemy_2> enemies_2;
        SoundEffect shotarrow, bonus, chestsound, killing, levelup, hurt, gameover;

        public Level(Game1 game, SpriteBatch spriteBatch)
        {
            this.game = game;
            this.spriteBatch = spriteBatch;
            cell = game.Content.Load<Texture2D>("cell");
            saw = game.Content.Load<Texture2D>("Saw");
            enemy1 = game.Content.Load<Texture2D>("Mace");
            shotarrow = game.Content.Load<SoundEffect>("shot");
            bonus = game.Content.Load<SoundEffect>("bonus");
            chestsound = game.Content.Load<SoundEffect>("chestsound");
            killing = game.Content.Load<SoundEffect>("killing");
            levelup = game.Content.Load<SoundEffect>("levelup");
            hurt = game.Content.Load<SoundEffect>("hurt");
            gameover = game.Content.Load<SoundEffect>("gameover");
            boxs = new List<Blocks>();
            abilities = new List<Blocks>();
            cells = new List<Cell>();
            chests = new List<Chest>();
            chests_new = new List<Chest>();
            enemies = new List<Enemy_1>();
            enemies_2 = new List<Enemy_2>();
        }

        public void Update(GameTime gameTime) 
        {
            if (lvl == 2 && enemies_2.Count == 0 && enemies.Count == 0&scores==6) 
            {
                foreach (Cell cell in cells)
                {
                cell.Remove();
                }
                cells.Clear();
                final();
            }

            if (scores == 8 && enemies_2.Count == 0 && enemies.Count == 0) 
            {
                foreach (Cell cell in cells)
                {
                    cell.Remove();
                }
                cells.Clear();
            }

            if (scores == 3&&lvl==1)
            {
                foreach (Cell cell in cells)
                {
                    cell.Remove();
                }
                cells.Clear();
            }
        }

        public void LevelOne() 
        {
            boxture1 = game.Content.Load<Texture2D>("ground");
            boxture2 = game.Content.Load<Texture2D>("leafground");
            rock = game.Content.Load<Texture2D>("rock");
            sunduk1 = game.Content.Load<Texture2D>("sunduk");
            sunduk2 = game.Content.Load<Texture2D>("sunduk2");
            arrowpic = game.Content.Load<Texture2D>("arrow");
            arrowpic2 = game.Content.Load<Texture2D>("flame");
            ability1 = game.Content.Load<Texture2D>("ability1");
            ability2 = game.Content.Load<Texture2D>("ability2");
            int x = 0, y = 0;
            int[,] map = new int[,]
            {{2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2}, 
             {1,1,1,1,1,1,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1}, 
             {1,1,1,1,1,0,0,0,0,0,1,1,0,0,0,1,3,0,0,0,0,0,0,0,0,5,0,0,1,1,1,1}, 
             {1,1,1,1,0,0,0,0,0,0,1,1,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,1,1}, 
             {1,1,1,0,0,0,0,0,0,0,1,1,0,0,0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,1,1}, 
             {1,1,0,0,0,0,1,0,0,0,1,1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1}, 
             {1,0,0,0,0,1,1,1,0,0,1,1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1}, 
             {1,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1}, 
             {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4}, 
             {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4}, 
             {1,0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,1,1,1,1,1,1,1,1,1,1,0,0,1,1,1}, 
             {1,0,0,0,0,1,1,1,0,0,1,1,1,1,1,0,0,1,1,0,0,0,0,0,0,0,1,0,0,1,1,1}, 
             {1,1,0,0,0,0,1,0,0,0,1,1,1,1,1,0,0,1,1,0,0,0,0,0,0,0,1,0,0,1,1,1}, 
             {1,1,1,0,0,0,0,0,0,0,1,1,0,0,0,0,0,1,1,0,0,1,0,0,0,3,1,0,0,1,1,1}, 
             {1,1,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1}, 
             {1,1,1,1,1,0,0,0,0,0,1,1,3,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1}, 
             {1,1,1,1,1,1,0,0,0,1,1,1,1,0,0,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,1},
             {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
             {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}};
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    Rectangle rect = new Rectangle(x, y, 40, 40);
                    int a = map[i, j];
                    switch (a)
                    {
                        case 1:
                            boxs.Add(new Blocks(this.game, spriteBatch, ref boxture1, new Point(rect.X, rect.Y)));
                            break;
                        case 2:
                            boxs.Add(new Blocks(this.game, spriteBatch, ref boxture2, new Point(rect.X, rect.Y)));
                            break;
                        case 3:
                            chests.Add(new Chest(this.game, spriteBatch, ref sunduk1, new Point(rect.X, rect.Y)));
                            break;
                        case 4:
                            cells.Add(new Cell(this.game, spriteBatch, ref cell, new Point(rect.X, rect.Y)));
                            break;
                        case 5:
                            abilities.Add(new Blocks(this.game, spriteBatch, ref ability1, new Point(rect.X, rect.Y)));
                            break;
                        default:
                            break;
                    }
                    x += 40;
                }
                x = 0;
                y += 40;
            }
            enemies.Add(new Enemy_1(this, this.game, spriteBatch, ref saw, new Point(340, 350)));
            enemies.Add(new Enemy_1(this, this.game, spriteBatch, ref saw, new Point(1100, 200)));
        }

        public void LevelTwo()
        {
            levelup.Play();
            lvl = 2;
            ClearLevel();            
            int x = 0, y = 0;
            int[,] map = new int[,]
            {{2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,4,4,2,2}, 
             {5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}, 
             {5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}, 
             {5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}, 
             {5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}, 
             {5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}, 
             {5,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1}, 
             {5,1,1,1,1,1,1,1,1,1,1,1,1,1,4,4,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}, 
             {0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}, 
             {0,0,0,1,3,0,0,0,0,0,0,0,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}, 
             {5,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,1}, 
             {5,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1}, 
             {5,0,0,1,0,0,0,0,0,0,0,0,0,0,0,8,0,0,0,0,7,0,0,0,0,0,0,0,1,0,0,1}, 
             {5,0,0,1,0,0,0,0,0,0,0,0,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,0,0,1}, 
             {5,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,1}, 
             {5,0,0,0,0,0,0,0,7,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,6,0,0,0,0,0,1}, 
             {5,0,0,0,0,0,0,0,0,0,0,0,0,0,3,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}, 
             {5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
             {5,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}};
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    Rectangle rect = new Rectangle(x, y, 40, 40);
                    int a = map[i, j];
                    switch (a)
                    {
                        case 1:
                            boxs.Add(new Blocks(this.game, spriteBatch, ref rock, new Point(rect.X, rect.Y)));
                            break;
                        case 2:
                            boxs.Add(new Blocks(this.game, spriteBatch, ref boxture2, new Point(rect.X, rect.Y)));
                            break;
                        case 3:
                            chests.Add(new Chest(this.game, spriteBatch, ref sunduk1, new Point(rect.X, rect.Y)));
                            break;
                        case 4:
                            cells.Add(new Cell(this.game, spriteBatch, ref cell, new Point(rect.X, rect.Y)));
                            break;
                        case 5:
                            boxs.Add(new Blocks(this.game, spriteBatch, ref boxture1, new Point(rect.X, rect.Y)));
                            break;
                        case 6:
                            enemies_2.Add(new Enemy_2(this, this.game, spriteBatch, ref enemy1, new Point(rect.X, rect.Y)));
                            break;
                        case 7:
                            enemies.Add(new Enemy_1(this, this.game, spriteBatch, ref saw, new Point(rect.X, rect.Y)));
                            break;
                        case 8:
                            abilities.Add(new Blocks(this.game, spriteBatch, ref ability2, new Point(rect.X, rect.Y)));
                            break;
                        default:
                            break;
                    }
                    x += 40;
                }
                x = 0;
                y += 40;
            }
        }

        protected void final() 
        {
            cells.Add(new Cell(this.game, spriteBatch, ref cell, new Point(1120, 0)));
            cells.Add(new Cell(this.game, spriteBatch, ref cell, new Point(1160, 0)));
            enemies_2.Add(new Enemy_2(this, this.game, spriteBatch, ref enemy1, new Point(80, 140)));
            enemies_2.Add(new Enemy_2(this, this.game, spriteBatch, ref enemy1, new Point(1160, 100)));
            enemies_2.Add(new Enemy_2(this, this.game, spriteBatch, ref enemy1, new Point(480, 180)));
            enemies_2.Add(new Enemy_2(this, this.game, spriteBatch, ref enemy1, new Point(680, 160)));
            enemies_2.Add(new Enemy_2(this, this.game, spriteBatch, ref enemy1, new Point(580, 100)));
            enemies.Add(new Enemy_1(this, this.game, spriteBatch, ref saw, new Point(200, 80)));
            enemies.Add(new Enemy_1(this, this.game, spriteBatch, ref saw, new Point(280, 160)));
            boss = new Enemy_3(this, this.game, spriteBatch, ref enemy1, new Point(1000, 130));
        }

        public void NewGameLevel() 
        {
            lvl = 1;            
            lives = livesfull;
            resname = null;
            scores = 0;
            arrowlvl = 0;
            timediff=0;
            time = 0;
            cldown = 0;
            ClearLevel();
            LevelOne();
        }

        public void WinGame() 
        {
            scoreres = scores;
            timeres = game.currentTime / 1000;
            if (game.nameNow != "")
                resname = game.nameNow;
            game.nameNow = "";
            if (lvl == 1)
            {
                winscore = 150 * timeres*(scoreres+1);
                if (resname == null)
                    resname = "player" + (winscore / 10).ToString();
            }
            else
            {
                levelup.Play();
                winscore = lives * (500 - timeres);
                if (resname == null)
                    resname = "player" + (winscore / 10).ToString();
            }
            lvl = 0;
            newres = 1;
        }

        public void ClearLevel() 
        {
            foreach (Chest sunduk in chests)
            {
                sunduk.Remove();
            }
            chests.Clear();
            foreach (Chest sunduk in chests_new)
            {
                sunduk.Remove();
            }
            chests_new.Clear();
            foreach (Blocks block in boxs)
            {
                block.Remove();
            }
            boxs.Clear();
            foreach (Blocks block in abilities)
            {
                block.Remove();
            }
            abilities.Clear();
            foreach (Cell rock in cells)
            {
                rock.Remove();
            }
            cells.Clear();
            foreach (Enemy_1 enem in enemies)
            {
                enem.Remove();
            }
            enemies.Clear();
            foreach (Enemy_2 enem in enemies_2)
            {
                enem.Remove();
            }
            enemies_2.Clear();
        }

        public void shot(Rectangle rect, bool direction) 
        {
            if (arrowlvl ==2&&cldown==1&&timediff<5000)
            {
                if (direction)
                    arrow2 = new Arrow_2(this.game, this, spriteBatch, ref arrowpic2, new Point(rect.X, rect.Y + 15), direction);
                else
                    arrow2 = new Arrow_2(this.game, this, spriteBatch, ref arrowpic2, new Point(rect.X + 25, rect.Y + 15), direction);
                shotarrow.Play();
            }
            else if (arrowlvl > 0)
            {
                if (direction)
                    arrow = new Arrow_1(this.game, this, spriteBatch, ref arrowpic, new Point(rect.X, rect.Y + 15), direction);
                else
                    arrow = new Arrow_1(this.game, this, spriteBatch, ref arrowpic, new Point(rect.X + 25, rect.Y + 15), direction);
                shotarrow.Play();
            }
        }

        public int timediff=0, time=0, cldown=0;

        public void cooldown() 
        {
                cldown = 1;
                if (timediff >= 0 && timediff < 5000)
                {
                    if (time == 0)
                    {
                        time = game.currentTime;
                        bonus.Play();
                    }
                    timediff = game.currentTime - time;
                }
                else if (timediff>5000&&timediff<=14000)
                {
                    timediff = game.currentTime - time;
                }
                else if (timediff > 14000)
                {
                    timediff = 0;
                    time = 0;
                    cldown = 0;
                }
        }

        public void Arrow_1Collides(Arrow_1 arrow, Rectangle rect) 
        {
            foreach (Enemy_1 enem in enemies)
            {
                if (enem.rec.Intersects(rect))
                {
                    enem.hp = enem.hp - 10;
                    if (enem.hp <= 0)
                    {
                        killing.Play();
                        enemies.Remove(enem);
                        enem.Remove();
                    }
                    arrow.Remove();
                    arrow = null;
                    break;
                }
            }
            if (arrow != null)
                foreach (Enemy_2 enem in enemies_2)
                {
                    if (enem.rec.Intersects(rect))
                    {
                        enem.hp = enem.hp - 8;
                        if (enem.hp <= 0)
                        {
                            killing.Play();
                            enemies_2.Remove(enem);
                            enem.Remove();
                        }
                        arrow.Remove();
                        arrow = null;
                        break;
                    }
                }
            if (arrow != null&&boss!=null)
                if (boss.rec.Intersects(rect))
                {
                    boss.hp = boss.hp - 5;
                    if (boss.hp <= 0)
                    {
                        killing.Play();
                        boss.Remove();
                        boss = null;
                    }
                    arrow.Remove();
                    arrow = null;
                }
            if (arrow != null)
                foreach (Blocks blocks in boxs)
                {
                    if (blocks.rec.Intersects(rect))
                    {
                        arrow.Remove();
                        arrow = null;
                        break;
                    }
                }
            if (arrow != null)
                foreach (Cell cell in cells)
                {
                    if (cell.rec.Intersects(rect))
                    {
                        arrow.Remove();
                        arrow = null;
                        break;
                    }
                }
        }

        public bool CollidesBox(Rectangle rect) // столкновение 
        {
            foreach (Blocks blocks in boxs)
            {
                if (blocks.rec.Intersects(rect))
                    return true;
            }
            return false;
        }

        public bool CollidesRock(Rectangle rect) 
        {
            foreach (Cell cell in cells)
            {
                if (cell.rec.Intersects(rect))
                    return true;
            }
            return false;
        }

        public bool CollidesChest(Rectangle rect)
        {
            foreach (Chest sunduk in chests)
            {
                if (sunduk.rec.Intersects(rect))
                {
                    chestsound.Play();
                    chests.Remove(sunduk);
                    sunduk.Remove();
                    Chest sunduknew = new Chest(this.game, spriteBatch, ref sunduk2, new Point(sunduk.rec.X, sunduk.rec.Y));
                    chests_new.Add(sunduknew);
                    scores++;
                    return true;
                }
            }
            return false;
        }

        public bool CollidesEnemy_1(Rectangle rect) // потеря жизней
        {
            foreach (Enemy_1 enem in enemies)
            {
                if (enem.rec.Intersects(rect))
                {
                    hurt.Play();
                    if (lives > 0)
                        lives--;
                    if (lives == 0)
                        gameover.Play();
                    return true;
                }                
            }
            return false;
        }

        public bool CollidesEnemy_2(Rectangle rect)
        {
            foreach (Enemy_2 enem in enemies_2)
            {
                enem.herorect(rect);
                if (enem.rec.Intersects(rect))
                {
                    hurt.Play();
                    if (lives > 0)
                        lives--;
                    if (lives == 0)
                        gameover.Play();
                    return true;
                }
            }
            if (boss != null)
            {
                boss.herorect(rect);
                if (boss.rec.Intersects(rect))
                {
                    hurt.Play();
                    if (lives > 0)
                        lives--;
                    if (lives == 0)
                        gameover.Play();
                    return true;
                }
            }
            return false;
        }

        public void Arrow_2Collides(Arrow_2 arrow, Rectangle rect)
        {
            foreach (Enemy_1 enem in enemies)
            {
                if (enem.rec.Intersects(rect))
                {
                    enem.hp = enem.hp - 30;
                    if (enem.hp <= 0)
                    {
                        killing.Play();
                        enemies.Remove(enem);
                        enem.Remove();
                    }
                    arrow.Remove();
                    arrow = null;
                    break;
                }
            }
            if (arrow != null)
                foreach (Enemy_2 enem in enemies_2)
                {
                    if (enem.rec.Intersects(rect))
                    {
                        enem.hp = enem.hp - 25;
                        if (enem.hp <= 0)
                        {
                            killing.Play();
                            enemies_2.Remove(enem);
                            enem.Remove();
                        }
                        arrow.Remove();
                        arrow = null;
                        break;
                    }
                }
            if (arrow != null && boss != null)
                if (boss.rec.Intersects(rect))
                {
                    boss.hp = boss.hp - 20;
                    if (boss.hp <= 0)
                    {
                        killing.Play();
                        boss.Remove();
                        boss = null;
                    }
                    arrow.Remove();
                    arrow = null;
                }
            if (arrow != null)
                foreach (Blocks blocks in boxs)
                {
                    if (blocks.rec.Intersects(rect))
                    {
                        arrow.Remove();
                        arrow = null;
                        break;
                    }
                }
            if (arrow != null)
                foreach (Cell cell in cells)
                {
                    if (cell.rec.Intersects(rect))
                    {
                        arrow.Remove();
                        arrow = null;
                        break;
                    }
                }
        }

        public void CollidesAbility(Rectangle rect)
        {
            foreach (Blocks blocks in abilities)
            {
                if (blocks.rec.Intersects(rect))
                {
                    if (lvl==1)
                        arrowlvl = 1;
                    if (lvl == 2)
                        arrowlvl = 2;
                    blocks.Remove();
                    bonus.Play();
                }
            }
            if(arrowlvl==1&&lvl==1)
                abilities.Clear();
            if (arrowlvl == 2 && lvl == 2)
                abilities.Clear();
        }
    }
}